package eu.specs.project.enforcement.broker.interfaces;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.jclouds.compute.RunNodesException;
import org.jclouds.compute.callables.ScriptStillRunningException;
import org.jclouds.scriptbuilder.domain.Statement;

import eu.specs.datamodel.broker.BrokerTemplateBean;
import eu.specs.datamodel.broker.ClusterNode;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodesInfo;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.project.enforcement.broker.exceptions.ComputationException;
import eu.specs.project.enforcement.broker.utility.ScriptExecutionResult;


public interface ComputeServiceInterface {
	
	public void enableProvider(String providerEndpoint, String defaultUser, ProviderCredential providerCredential) throws ComputationException;

	public NodesInfo getNodesByGroup(String groupname, NodeCredential nodeCredential) throws ComputationException;
	public NodesInfo getNodesByGroup(String groupname) throws ComputationException;
	
	public NodesInfo createNodesInGroup(String groupName, int numberOfInstances, BrokerTemplateBean descriptor, NodeCredential nodeCredential, int... inboudports)  throws NoSuchElementException, RunNodesException, Exception;	
	
	public ClusterNode addNode(String groupName, NodeCredential nodeCredential, String privateAccessKey) throws RunNodesException;
    
	public ScriptExecutionResult addNewUser(String userName, String password, String newPublicKey, String newPrivateKey, List<ClusterNode> nodes, String privateKeyDefaultUser) throws ComputationException;    
    
    public ScriptExecutionResult executeScriptOnNode(String user, ClusterNode node, String[] instructions, String privateKey, boolean sudo) throws NoSuchElementException, IllegalStateException, ScriptStillRunningException;
    public ScriptExecutionResult executeStatementOnNode(String user, ClusterNode node, Statement s, String privateKey, boolean sudo) throws ComputationException;
    
    public Thread threadExecuteStatementsOnNode(String user, ClusterNode node, Statement statement, String privateKey, boolean sudo) throws ComputationException;
    public Thread threadExecuteInstructionsOnNode(String user, ClusterNode node, String[] istructions, String privateKey, boolean sudo) throws ComputationException;
    
    public Map<String, ScriptExecutionResult> executeScriptOnNodes(String user, List<ClusterNode> nodes, String[] instructions, String privateKey, boolean sudo) throws ComputationException;
    public Map<String, ScriptExecutionResult> executeScriptOnNodes(String user, List<ClusterNode> nodes,Statement s, String privateKey, boolean sudo) throws ComputationException;
    
    public void suspendNodesInGroup(List<ClusterNode> hosts);
    public void resumeNodesInGroup(List<ClusterNode> hosts);
    public void rebootNodesInGroup(List<ClusterNode> hosts);
    public void destroyCluster(List<ClusterNode> hosts);
    public void destroyClusterWithName(String groupName);
    public void destroySingleNode(ClusterNode node);



}