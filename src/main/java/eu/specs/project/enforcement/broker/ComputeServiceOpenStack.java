/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Implement the funcionalities to create and manage the nodes in a Cloud Service Provide
using the jcloud library

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker;

import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.getOnlyElement;
import static org.jclouds.compute.config.ComputeServiceProperties.TIMEOUT_SCRIPT_COMPLETE;
import static org.jclouds.compute.options.TemplateOptions.Builder.overrideLoginCredentials;
import static org.jclouds.compute.predicates.NodePredicates.TERMINATED;
import static org.jclouds.compute.predicates.NodePredicates.inGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import javax.management.RuntimeErrorException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jclouds.Constants;
import org.jclouds.ContextBuilder;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.RunNodesException;
import org.jclouds.compute.callables.ScriptStillRunningException;
import org.jclouds.compute.domain.ComputeMetadata;
import org.jclouds.compute.domain.ExecResponse;
import org.jclouds.compute.domain.Hardware;
import org.jclouds.compute.domain.Image;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.compute.domain.Template;
import org.jclouds.compute.domain.TemplateBuilder;
import org.jclouds.compute.functions.Sha512Crypt;
import org.jclouds.domain.Location;
import org.jclouds.domain.LoginCredentials;
import org.jclouds.openstack.keystone.v2_0.config.CredentialTypes;
import org.jclouds.openstack.keystone.v2_0.config.KeystoneProperties;
import org.jclouds.openstack.neutron.v2.NeutronApi;
import org.jclouds.openstack.neutron.v2.domain.ExternalGatewayInfo;
import org.jclouds.openstack.neutron.v2.domain.FloatingIP;
import org.jclouds.openstack.neutron.v2.domain.Network;
import org.jclouds.openstack.neutron.v2.domain.Network.CreateNetwork;
import org.jclouds.openstack.neutron.v2.domain.NetworkStatus;
import org.jclouds.openstack.neutron.v2.domain.Port;
import org.jclouds.openstack.neutron.v2.domain.Router;
import org.jclouds.openstack.neutron.v2.domain.Router.CreateRouter;
import org.jclouds.openstack.neutron.v2.domain.Rule.CreateRule;
import org.jclouds.openstack.neutron.v2.domain.RuleDirection;
import org.jclouds.openstack.neutron.v2.domain.RuleEthertype;
import org.jclouds.openstack.neutron.v2.domain.RuleProtocol;
import org.jclouds.openstack.neutron.v2.domain.SecurityGroup;
import org.jclouds.openstack.neutron.v2.domain.SecurityGroup.CreateSecurityGroup;
import org.jclouds.openstack.neutron.v2.domain.Subnet;
import org.jclouds.openstack.neutron.v2.domain.Subnet.CreateSubnet;
import org.jclouds.openstack.neutron.v2.extensions.FloatingIPApi;
import org.jclouds.openstack.neutron.v2.extensions.RouterApi;
import org.jclouds.openstack.neutron.v2.extensions.SecurityGroupApi;
import org.jclouds.openstack.neutron.v2.features.NetworkApi;
import org.jclouds.openstack.neutron.v2.features.PortApi;
import org.jclouds.openstack.neutron.v2.features.SubnetApi;
import org.jclouds.openstack.nova.v2_0.NovaApi;
import org.jclouds.openstack.nova.v2_0.compute.options.NovaTemplateOptions;
import org.jclouds.openstack.nova.v2_0.domain.KeyPair;
import org.jclouds.openstack.nova.v2_0.extensions.KeyPairApi;
import org.jclouds.scriptbuilder.ScriptBuilder;
import org.jclouds.scriptbuilder.domain.LiteralStatement;
import org.jclouds.scriptbuilder.domain.Statement;
import org.jclouds.scriptbuilder.statements.login.UserAdd;
import org.jclouds.scriptbuilder.statements.ssh.InstallRSAPrivateKey;
import org.jclouds.ssh.jsch.config.JschSshClientModule;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Module;

import eu.specs.datamodel.broker.BrokerTemplateBean;
import eu.specs.datamodel.broker.ClusterNode;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodesInfo;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.project.enforcement.broker.exceptions.ComputationException;
import eu.specs.project.enforcement.broker.interfaces.ComputeServiceInterface;
import eu.specs.project.enforcement.broker.utility.ScriptExecutionResult;
import eu.specs.project.enforcement.broker.utility.ScriptExecutionResult.ScriptExecutionOutcome;


public class ComputeServiceOpenStack implements ComputeServiceInterface{
	private ComputeServiceContext context;
	private ComputeService compute;
	private String defaultUser;
	private Template template;
	private String globalNetworkId;
	private NovaApi novaApi;
	private NeutronApi neutronApi;
	private static final Logger logger = LogManager.getLogger(ComputeServiceOpenStack.class);

	public ComputeServiceOpenStack(){
		super();
	}

	/**
	 * Represents a Cloud Service Provider, it give access to creation and management of nodes.
	 * It uses jcloud to provide service over different cloud platforms
	 * @param providerAddress address of the provider 
	 * @param defaultUser 
	 * @param providerCredential
	 */
	public ComputeServiceOpenStack(String providerAddress, String defaultUser, ProviderCredential providerCredential){

		Properties properties = new Properties();
		long scriptTimeout = TimeUnit.MILLISECONDS.convert(20, TimeUnit.MINUTES);
		properties.setProperty(TIMEOUT_SCRIPT_COMPLETE, Long.toString(scriptTimeout));
		this.defaultUser = defaultUser;

		context = ContextBuilder.newBuilder("openstack-nova")
				.endpoint(providerAddress)
				.credentials(providerCredential.getUsername(), providerCredential.getPassword())
				.overrides(properties)
				.modules(ImmutableSet.<Module> of(
						new JschSshClientModule()))
						.buildView(ComputeServiceContext.class); 

		compute = context.getComputeService();

		Properties overrides = new Properties();
		overrides.setProperty(KeystoneProperties.CREDENTIAL_TYPE, CredentialTypes.PASSWORD_CREDENTIALS);
		overrides.setProperty(Constants.PROPERTY_RELAX_HOSTNAME, "true");
		overrides.setProperty(Constants.PROPERTY_TRUST_ALL_CERTS, "true");

		novaApi = context.unwrapApi(NovaApi.class);


		//instance for making request to the neutron component "network component"
		neutronApi = ContextBuilder.newBuilder("openstack-neutron")
				.endpoint(providerAddress)
				.credentials(providerCredential.getUsername(), providerCredential.getPassword())
				//.modules(modules)
				.overrides(overrides)
				.buildApi(NeutronApi.class);  
	}

	@Override
	public void enableProvider(String providerEndpoint, String defaultUser, ProviderCredential providerCredential) throws ComputationException{
		Properties properties = new Properties();
		long scriptTimeout = TimeUnit.MILLISECONDS.convert(20, TimeUnit.MINUTES);
		properties.setProperty(TIMEOUT_SCRIPT_COMPLETE, Long.toString(scriptTimeout));
		this.defaultUser = defaultUser;

		context = ContextBuilder.newBuilder("openstack-nova")
				.endpoint(providerEndpoint)
				.credentials(providerCredential.getUsername(), providerCredential.getPassword())
				.overrides(properties)
				.modules(ImmutableSet.<Module> of(
						new JschSshClientModule()))
						.buildView(ComputeServiceContext.class); 

		compute = context.getComputeService();    


		Properties overrides = new Properties();
		overrides.setProperty(KeystoneProperties.CREDENTIAL_TYPE, CredentialTypes.PASSWORD_CREDENTIALS);
		overrides.setProperty(Constants.PROPERTY_RELAX_HOSTNAME, "true");
		overrides.setProperty(Constants.PROPERTY_TRUST_ALL_CERTS, "true");

		novaApi = context.unwrapApi(NovaApi.class);

		//instance for making request to the neutron component "network component"
		neutronApi = ContextBuilder.newBuilder("openstack-neutron")
				.endpoint(providerEndpoint)
				.credentials(providerCredential.getUsername(), providerCredential.getPassword())
				//.modules(modules)
				.overrides(overrides)
				.buildApi(NeutronApi.class);  

		deallocateUnusedFloatingIP();

	}


	private Template obtainTemplate(BrokerTemplateBean descriptor, String publicKey, String networkId, String groupName){
		TemplateBuilder templateBuilder = compute.templateBuilder();

		NovaTemplateOptions options = NovaTemplateOptions.Builder.blockOnPort(22, 10000);

		options.keyPairName(publicKey);
		options.securityGroups(groupName);
		options.autoAssignFloatingIp(true);
		options.networks(networkId);

		template = templateBuilder
				.imageId(descriptor.getAppliance())
				.hardwareId(descriptor.getHw())
				.locationId(descriptor.getZone())
				.options(options)
				.build();

		return template;
	}



	/**
	 * creates multiple nodes in a Cloud Service Provider
	 * @param groupName string that represent the group to which the machines are assigned 
	 * @param numberOfInstances number of instances to create 
	 * @param descriptor object that contain information about the appliance, hw and zone
	 * @param nodeCredential object containing root password private and public key
	 * @param inboudports inbound ports to open on the created machines 
	 * @return information about all the nodes created and the credentials
	 * @throws NoSuchElementException
	 * @throws Exception
	 */
	@Override
	public NodesInfo createNodesInGroup(String groupName, int numberOfInstances, BrokerTemplateBean descriptor, NodeCredential nodeCredential, int... inboudports)  throws NoSuchElementException, RunNodesException, Exception {	

		String privateAccessKey=nodeCredential.getPrivatekey();
		Set<? extends NodeMetadata> nodes = null;
		try {
			createSecurityGroup(groupName, "Security group created for SPECS", inboudports);
			Thread.sleep(3000);	
			globalNetworkId = createNetwork(groupName);
			int subnetIp=ThreadLocalRandom.current().nextInt(1, 255 + 1);
			String strSubnetIp=Integer.toString(subnetIp);
			String subnetId = createSubnet(groupName, globalNetworkId, "192.168."+strSubnetIp+".0/24","192.168."+strSubnetIp+".1");
			connectNetworkToExtern(groupName, subnetId);
			Thread.sleep(3000);	
			KeyPair keyPair = this.createKeyPair(groupName, nodeCredential.getPublickey());
			Thread.sleep(3000);	
			template = obtainTemplate(descriptor, keyPair.getName(), globalNetworkId, groupName);


			nodes = compute.createNodesInGroup(groupName, numberOfInstances, template);

			Thread.sleep(3000);		

		}catch(NoSuchElementException e){
			throw e;
		}
		catch (Exception e) {
			destroyClusterWithName(groupName);
			logger.trace(e);
			throw e;
		}
		NodesInfo info = new NodesInfo();
		for(NodeMetadata n : nodes){
			info.addNode(new ClusterNode(n.getId(), 
					n.getPublicAddresses().isEmpty() ? "" : n.getPublicAddresses().iterator().next(),
							n.getPrivateAddresses().isEmpty() ? "" : n.getPrivateAddresses().iterator().next()
					));
		}

		ScriptBuilder sb = new ScriptBuilder();
		ExecResponse er = null;

		//Test the connection, wait until the sistem can connect to the SSH using the private key
		logger.debug("first ssh connection to VM failed");
		long milliseconds = 10000;
		LiteralStatement sampleCommand = new LiteralStatement(new String("ls"));
		sb.addStatement(sampleCommand);

		for(NodeMetadata n : nodes){
			for(int i=0;i<10;i++){
				try {
					compute.runScriptOnNode(n.getId(), sb, 
							overrideLoginCredentials(getLoginForCommandExecution(this.defaultUser, privateAccessKey)).runAsRoot(true).wrapInInitScript(false));
					Thread.sleep(milliseconds);
					break;
				} catch (Exception e1) {
					logger.debug("ssh connection to VM failed");
					logger.trace(e1);
					if(i==9){
						logger.debug("ssh connection to VM failed");
					}
				}
			}
		}

		if (!("root").equalsIgnoreCase(this.defaultUser)){
			sb = new ScriptBuilder();
			LiteralStatement enableRootUser = new LiteralStatement(new String("cp /home/"+this.defaultUser+"/.ssh/authorized_keys /root/.ssh/"));
			sb.addStatement(enableRootUser);

			for(NodeMetadata n : nodes){
				try{
					er = compute.runScriptOnNode(n.getId(), sb, 
							overrideLoginCredentials(getLoginForCommandExecution(this.defaultUser, privateAccessKey)).runAsRoot(true).wrapInInitScript(false));
				}catch(Exception e){
					logger.trace(e);
					throw e;
				}
				if(er.getExitStatus()!=0){
					destroyClusterWithName(groupName);
					throw new RuntimeErrorException(new Error(er.getOutput()));
				}
			}
			this.defaultUser="root";

		}


		InstallRSAPrivateKey instPriv = new InstallRSAPrivateKey(nodeCredential.getPrivatekey());

		sb = new ScriptBuilder();
		sb.addStatement(instPriv);


		for(NodeMetadata n : nodes){
			try {
				er = compute.runScriptOnNode(n.getId(), sb, overrideLoginCredentials(getLoginForCommandExecution(this.defaultUser, privateAccessKey)).runAsRoot(false).wrapInInitScript(false));

			}catch(Exception exception){
				logger.trace(exception);
				throw exception;
			}
			//verify if the statement execute with success otherwise destroy the cluster
			if(er.getExitStatus()!=0){
				destroyClusterWithName(groupName);
				throw new RuntimeErrorException(new Error("Error in configuration of custom RSA keys"));
			}
		}


		//assign a new password to the default user, the password is the same for all the nodes

		LiteralStatement enablePasswordSsh = new LiteralStatement(new String("exec 3<> /etc/ssh/sshd_config && awk '{ gsub(\"PasswordAuthentication\",\"#PasswordAuthentication\"); print }' /etc/ssh/sshd_config >&3 hash service 2>&- && (service ssh reload 2>&- || /etc/init.d/ssh* reload) || rcsshd restart"));

		sb = new ScriptBuilder();

		sb.addStatement(enablePasswordSsh);		

		for(NodeMetadata n : nodes){
			try{
				er = compute.runScriptOnNode(n.getId(), sb, 
						overrideLoginCredentials(getLoginForCommandExecution(this.defaultUser, privateAccessKey)).runAsRoot(true).wrapInInitScript(false));
			}catch(Exception e){
				logger.trace(e);
				throw e;
			}
			if(er.getExitStatus()!=0){
				destroyClusterWithName(groupName);
				throw new RuntimeErrorException(new Error(er.getOutput()));
			}
		}
		this.defaultUser="root";
		info.setPublicKey(nodeCredential.getPublickey());
		info.setPrivateKey(nodeCredential.getPrivatekey());
		info.setRootPassword(nodeCredential.getRootpassword());
		return info;
	}

	@Override
	public ClusterNode addNode(String groupName, NodeCredential nodeCredential, String privateAccessKey) throws RunNodesException{
		NodeMetadata node = null;
		try{
			//use the template for generate a new node in the group
			node = getOnlyElement(compute.createNodesInGroup(groupName, 1, template));
		} catch (RunNodesException e) {
			destroyClusterWithName(groupName);
			throw e;
		}

		InstallRSAPrivateKey instPriv = new InstallRSAPrivateKey(nodeCredential.getPrivatekey());
		ScriptBuilder sb = new ScriptBuilder();
		sb.addStatement(instPriv);

		ExecResponse er = null;
		try{
			er = compute.runScriptOnNode(node.getId(), sb, 
					overrideLoginCredentials(getLoginForCommandExecution(this.defaultUser, privateAccessKey)).runAsRoot(false).wrapInInitScript(false));
		}catch(Exception e){
			throw e;
		}

		if(er.getExitStatus()!=0){
			destroyClusterWithName(groupName);
			throw new RuntimeErrorException(new Error("Error in configuration of custom RSA keys"));
		}

		//assign a new password to the default user, the password is the same for all the nodes
		LiteralStatement pass = new LiteralStatement(String.format("echo -e \"%s\n%s\n\" | passwd root", nodeCredential.getRootpassword(),nodeCredential.getRootpassword()));
		sb = new ScriptBuilder();

		sb.addStatement(pass);
		try{
			er = compute.runScriptOnNode(node.getId(), sb, 
					overrideLoginCredentials(getLoginForCommandExecution(this.defaultUser, privateAccessKey)).runAsRoot(true).wrapInInitScript(false));
		}catch(Exception e){
			logger.trace(e);
			throw new RuntimeErrorException(new Error(er.getOutput()));
		}
		if(er.getExitStatus()!=0){
			destroyClusterWithName(groupName);
			throw new RuntimeErrorException(new Error("Error in installing root password and enabling it for SSH"));
		}

		return new ClusterNode(node.getId(), node.getPublicAddresses().iterator().next(), node.getPrivateAddresses().iterator().next());
	}



	/**
	 * 
	 * @param userName
	 * @param password
	 * @param newPublicKey
	 * @param newPrivateKey
	 * @param nodes
	 * @param privateKeyDefaultUser
	 * @return
	 */
	@Override
	public ScriptExecutionResult addNewUser(String userName, String password, String newPublicKey, String newPrivateKey, List<ClusterNode> nodes, String privateKeyDefaultUser) throws ComputationException{
		HashMap<String, String> info = new HashMap<String, String>();
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		UserAdd.Builder userBuilder = UserAdd.builder();
		userBuilder.login(userName);
		userBuilder.authorizeRSAPublicKey(newPublicKey);
		userBuilder.installRSAPrivateKey(newPrivateKey);
		userBuilder.defaultHome("/home");
		if(password!=null){
			userBuilder.password(password);
		}
		Statement userBuilderStatement = userBuilder.cryptFunction(Sha512Crypt.function()).build();
		ExecResponse er = null;

		for(ClusterNode n : nodes){
			try{
				er = compute.runScriptOnNode(n.getId(), userBuilderStatement,
						overrideLoginCredentials(getLoginForCommandExecution(this.defaultUser, privateKeyDefaultUser)).runAsRoot(true).wrapInInitScript(false)); 
			}catch (Exception e){
				logger.trace(e);
				throw new RuntimeErrorException(new Error(er.getOutput()));
			}

			if(er.getExitStatus()!=0){
				scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
				scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
				scriptResult.addNodeWithError(n);
				throw new RuntimeErrorException(new Error("Error in creating the user "+ userName));
			}
		}
		info.put("privateKeyNewUser", newPrivateKey);
		info.put("publicKeyNewUser", newPublicKey);
		info.put("username", userName);
		info.put("password", password);
		scriptResult.setAdditionalInfo(info);
		return scriptResult;
	}



	@Override
	public ScriptExecutionResult executeScriptOnNode(String user, ClusterNode node, String[] instructions, String privateKey, boolean sudo) throws NoSuchElementException, IllegalStateException, ScriptStillRunningException{
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		ScriptBuilder sb = new ScriptBuilder();
		for(String s : instructions){
			sb.addStatement(new LiteralStatement(s));
		}
		ExecResponse er = null;
		try{
			er = compute.runScriptOnNode(node.getId(), sb,
					overrideLoginCredentials(getLoginForCommandExecution(user, privateKey)).runAsRoot(sudo).wrapInInitScript(false));
		}catch(Exception e){
			logger.trace(e);

			throw new RuntimeErrorException(new Error(er.getOutput()));
		}
		if(er.getExitStatus()!=0){
			scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
			scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
			scriptResult.addNodeWithError(node);
			logger.debug(er.getOutput());
			logger.debug(er.getError());
			throw new RuntimeErrorException(new Error(er.getOutput()));
		}
		scriptResult.addOutput(node.getId(), er.getOutput());
		return scriptResult;
	}

	@Override
	public ScriptExecutionResult executeStatementOnNode(String user, ClusterNode node, Statement s, String privateKey, boolean sudo) throws ComputationException{
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		ScriptBuilder sb = new ScriptBuilder();
		sb.addStatement(s);
		ExecResponse er = null;
		try{
			er = compute.runScriptOnNode(node.getId(), sb,
					overrideLoginCredentials(getLoginForCommandExecution(user, privateKey)).runAsRoot(sudo).wrapInInitScript(false));
		}catch (Exception e){
			logger.trace(e);
			throw e;
		}
		if(er.getExitStatus()!=0){
			scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
			scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
			scriptResult.addNodeWithError(node);
			throw new RuntimeErrorException(new Error("Error in executing the script"));
		}
		scriptResult.addOutput(node.getId(), er.getOutput());
		return scriptResult;
	}

	@Override
	public Map<String, ScriptExecutionResult> executeScriptOnNodes(String user, List<ClusterNode> nodes, String[] instructions, String privateKey, boolean sudo) throws ComputationException{
		Map<String, ScriptExecutionResult> map = new HashMap<String, ScriptExecutionResult>();
		ScriptExecutionResult temp = null;
		for(ClusterNode n : nodes){
			try{
				temp = executeScriptOnNode(user, n, instructions, privateKey, sudo);
			}catch (Exception e){
				logger.trace(e);
				throw new ComputationException(e.getMessage());
			}
			map.put(n.getId(), temp);
		}
		return map;
	}

	@Override
	public Map<String, ScriptExecutionResult> executeScriptOnNodes(String user, List<ClusterNode> nodes,Statement s, String privateKey, boolean sudo) throws ComputationException{
		Map<String, ScriptExecutionResult> map = new HashMap<String, ScriptExecutionResult>();
		ScriptExecutionResult temp;
		for(ClusterNode n : nodes){
			try{
				temp = executeStatementOnNode(user, n, s, privateKey, sudo);
			}catch(Exception e){
				logger.trace(e);
				throw e;
			}

			map.put(n.getId(), temp);
		}
		return map;
	}


	private List<Hardware> getHardwareTypes(){

		Set<? extends Hardware> flavors = compute.listHardwareProfiles();
		List<Hardware> listHardware = new ArrayList<Hardware>(flavors);
		logger.debug("Flavor List");
		for (Hardware flavor : flavors){
			logger.debug("Flavor id : "+ flavor.getId());
			logger.debug("Flavor name : "+ flavor.getName());
			logger.debug("Flavor prov : "+ flavor.getProviderId());
		}
		return listHardware;
	}

	private List<Image> getAppliances(){

		Set<? extends Image> images = compute.listImages();
		List<Image> listImage = new ArrayList<Image>(images);
		logger.debug("Image List");
		for (Image image : images){
			logger.debug("Image id : "+ image.getId());
			logger.debug("Image name : "+ image.getName());
			logger.debug("Image desc : "+ image.getDescription());
		}
		return listImage;
	}

	private List<Location> getAvailabilityZones(){

		Set<? extends Location> locations = compute.listAssignableLocations();
		List<Location> listLocation = new ArrayList<Location>(locations);
		logger.debug("Location List");
		for (Location location : locations){
			logger.debug("Location id : "+ location.getId());
			logger.debug("Location desc : "+ location.getDescription());
		}
		return listLocation;
	}

	@Override
	public void suspendNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.suspendNode(hosts.get(i).getId());
		}
	}
	@Override
	public void resumeNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.resumeNode(hosts.get(i).getId());
		}
	}
	@Override
	public void rebootNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.rebootNode(hosts.get(i).getId());
		}
	}

	@Override
	public void destroyCluster(List<ClusterNode> hosts){ //distrugge anche keypair associati e il security group se non ci sono altre risorse pendenti associate

		for (int i = 0; i<hosts.size(); i++){
			compute.destroyNode(hosts.get(i).getId());	
		}

	}

	@Override
	public void destroyClusterWithName(String groupName){
		try{
			clearGroupNodesAndNetwork(groupName, groupName, groupName);
		} catch(Exception e){
			logger.trace(e);
		}
	}
	@Override
	public void destroySingleNode(ClusterNode node){
		compute.destroyNode(node.getId());
	}

	private LoginCredentials getLoginForCommandExecution(String user, String credential) {

		return LoginCredentials.builder().noPassword().user(user).credential(credential).build();   
	}

	@Override
	public Thread threadExecuteStatementsOnNode(String user, ClusterNode node, Statement statement, String privateKey, boolean sudo){
		//user, node,istructions,privateKey,sudo
		return new ExecuteStatementsOnNode(user, node, statement, privateKey, sudo, this);
	}

	private class ExecuteStatementsOnNode  extends Thread{

		private ExecuteStatementsOnNode(String user, ClusterNode node,
				Statement statement, String privateKey, boolean sudo,
				ComputeServiceOpenStack compute) {
			super();
			this.user = user;
			this.node = node;
			this.statement = statement;
			this.privateKey = privateKey;
			this.sudo = sudo;
			this.compute = compute;
		}

		private String user;
		private ClusterNode node;
		private Statement statement;
		private String privateKey;
		boolean sudo;
		private ComputeServiceOpenStack compute;

		@Override
		public void run(){ 

			try {
				compute.executeStatementOnNode(user, node,statement,privateKey,sudo);
			} catch (Exception e) {
				logger.trace(e);
			}

		}
	}
	@Override
	public Thread threadExecuteInstructionsOnNode(String user, ClusterNode node, String[] istructions, String privateKey, boolean sudo){
		//user, node,istructions,privateKey,sudo
		return new ExecuteIstructionsOnNode(user, node, istructions, privateKey, sudo, this);
	}

	private class ExecuteIstructionsOnNode  extends Thread{

		private String user;
		private ClusterNode node;
		private String[] istructions;
		private String privateKey;
		boolean sudo;
		private ComputeServiceOpenStack compute;

		private ExecuteIstructionsOnNode(String user, ClusterNode node,
				String[] istructions, String privateKey, boolean sudo,
				ComputeServiceOpenStack compute) {
			super();
			this.user = user;
			this.node = node;
			this.istructions = istructions;
			this.privateKey = privateKey;
			this.sudo = sudo;
			this.compute = compute;
		}
		@Override
		public void run(){ 

			try {
				compute.executeScriptOnNode(user, node,istructions,privateKey,sudo);
			} catch (Exception e) {
				logger.trace(e);
			}

		}
	}


	private void deleteKeypair(String keyPairName) throws RuntimeException{
		try{
			KeyPairApi keyPairApi = novaApi.getKeyPairApi("fr1").get();
			keyPairApi.delete(keyPairName);
			logger.debug("Deleted key : " + keyPairName);
		}catch(Exception e){
			logger.debug("Cannot delete key : " + keyPairName);
			logger.trace(e);
			throw e;
		}



	}

	private void deleteSecurityGroup(String securityGroupName) throws RuntimeException{
		try{
			SecurityGroupApi securityGroupApi = neutronApi.getSecurityGroupApi("fr1").get();
			ImmutableList<SecurityGroup> securityGroups = securityGroupApi.listSecurityGroups().concat().toList();
			for (SecurityGroup securityGroupElement : securityGroups){
				if (securityGroupElement.getName().equalsIgnoreCase(securityGroupName)){
					logger.debug("Delete Security Group : " + securityGroupElement.getName());
					securityGroupApi.deleteSecurityGroup(securityGroupElement.getId());
				}
			}
		}catch(Exception e){
			logger.debug("Cannot delete security group : " + securityGroupName);
			logger.trace(e);
			throw e;
		}

	}

	private KeyPair createKeyPair(String name, String publickey) throws RuntimeException{
		try{
			KeyPairApi keyPairApi = novaApi.getKeyPairApi("fr1").get();
			KeyPair keyPair = keyPairApi.createWithPublicKey(name, publickey);

			while(keyPairApi.get(name)==null){
				Thread.sleep(2000);	
			}
			while(!(keyPairApi.get(name).getPublicKey().equalsIgnoreCase(publickey))){
				Thread.sleep(2000);	
			}

			return keyPair;
		}catch(Exception e){
			logger.trace(e);
			throw new RuntimeErrorException(new Error(e.getMessage()));
		}
	}

	private boolean createSecurityGroup(String groupName, String description, int[] inboudports) throws RuntimeException{
		try{
			SecurityGroupApi securityGroupApi = neutronApi.getSecurityGroupApi("fr1").get();
			CreateSecurityGroup createSecurityGroup = CreateSecurityGroup.createBuilder()
					.description(description)
					.name(groupName)
					.build();
			SecurityGroup securityGroup = securityGroupApi.create(createSecurityGroup);
			//Ingress TCP rule
			for (int i=0; i<inboudports.length;i++){
				CreateRule createRule = CreateRule.createBuilder(RuleDirection.INGRESS, securityGroup.getId())
						.ethertype(RuleEthertype.IPV4).portRangeMin(inboudports[i]).portRangeMax(inboudports[i]).remoteIpPrefix("0.0.0.0/0").protocol(RuleProtocol.TCP)
						.build();
				securityGroupApi.create(createRule);

			}
			while(securityGroupApi.getSecurityGroup(securityGroup.getId()) == null){
				Thread.sleep(2000);	
			}
			securityGroupApi.getSecurityGroup(securityGroup.getId());			

		}catch(Exception e){
			logger.debug("Cannot create security group : " + groupName);
			logger.trace(e);
			throw new RuntimeErrorException(new Error(e.getMessage()));
		}
		return true;

	}

	private boolean deleteNetwork(String networkName) throws RuntimeErrorException{
		try{
			NetworkApi networkApi = neutronApi.getNetworkApi("fr1");
			List<Network> networkList = networkApi.list().concat().toList();
			for (Network network : networkList){
				if (network.getName().equalsIgnoreCase(networkName)){
					networkApi.delete(network.getId());
				}
			}

			return true;
		}catch (Exception e){
			logger.debug("Cannot remove the network "+networkName);
			logger.trace(e);
			throw e;
		}

	}

	private boolean deleteRouter(String routerName, String subnetName) throws RuntimeErrorException{
		try{
			String subnetId = null;
			NetworkApi networkApi = neutronApi.getNetworkApi("fr1");
			List<Network> networkList = networkApi.list().concat().toList();
			for (Network network : networkList){
				if (network.getName().equalsIgnoreCase(subnetName)){
					subnetId = network.getId();
				}
			}

			PortApi portApi = neutronApi.getPortApi("fr1");
			List<Port> portList = portApi.list().concat().toList();

			RouterApi routerApi = neutronApi.getRouterApi("fr1").get();
			List<Router> routerList = routerApi.list().concat().toList();
			for (Router router : routerList){
				if (router.getName().equalsIgnoreCase(routerName)){	

					//routerApi.removeInterfaceForSubnet(router.getId(), subnetId);

					String routerId = router.getId();

					for (Port port : portList){
						if (port.getDeviceId().equalsIgnoreCase(routerId)){

							routerApi.removeInterfaceForPort(routerId, port.getId());

							portApi.delete(port.getId());

						}
					}

					routerApi.delete(router.getId());
				}
			}
			return true;
		}catch (Exception e){
			logger.debug("Cannot remove the router "+routerName);
			logger.trace(e);
			throw e;
		}

	}

	private void connectNetworkToExtern(String routerName, String networkId) throws RuntimeErrorException{
		try{
			String externalAddressId = getExternalAddressId();

			RouterApi routerApi = neutronApi.getRouterApi("fr1").get();
			ExternalGatewayInfo externalGatewayInfo = ExternalGatewayInfo.builder().networkId(externalAddressId).build();
			CreateRouter createRouter = CreateRouter.createBuilder().name(routerName).adminStateUp(true)
					.externalGatewayInfo(externalGatewayInfo).build();

			Router router = routerApi.create(createRouter);
			while(routerApi.get(router.getId()) == null){
				Thread.sleep(2000);	
			}
			while(routerApi.get(router.getId()).getStatus() != NetworkStatus.ACTIVE){
				Thread.sleep(2000);	
			}

			routerApi.addInterfaceForSubnet(router.getId(), networkId);
			while(routerApi.get(router.getId()).getStatus() != NetworkStatus.ACTIVE){
				Thread.sleep(2000);	
			}


		}catch(Exception e){
			logger.debug("Cannot connect the subnet to the external network");
			logger.trace(e);
			throw new RuntimeErrorException(new Error(e.getMessage()));
		}


	}

	private String createNetwork(String networkName) throws RuntimeErrorException{
		NetworkApi networkApi = neutronApi.getNetworkApi("fr1");
		Network network = null;
		try{		
			CreateNetwork createNetwork = Network.createBuilder(networkName).adminStateUp(true).build();
			network = networkApi.create(createNetwork);
			String netId=network.getId();
			while(networkApi.get(netId).getStatus() != NetworkStatus.ACTIVE){
				Thread.sleep(2000);	
			}
			return netId;

		}catch (Exception e){
			logger.trace(e);
			throw new RuntimeErrorException(new Error(e.getMessage()));
		} 

	}

	private String createSubnet(String subnetName, String networkId, String cidr, String gatewayIp) throws RuntimeErrorException{
		SubnetApi subnetApi = neutronApi.getSubnetApi("fr1");
		NetworkApi networkApi = neutronApi.getNetworkApi("fr1");
		Network network = null;
		Subnet subnet = null;
		try{
			CreateSubnet createSubnet = CreateSubnet.createBuilder(networkId, cidr).gatewayIp(gatewayIp).name(subnetName).ipVersion(4).enableDhcp(true).build();//.dnsNameServers(dnsServers).build();
			subnet=subnetApi.create(createSubnet);
			String subnetId=subnet.getId();
			while(networkApi.get(networkId) == null){
				Thread.sleep(2000);	
			}	
			while(networkApi.get(networkId).getStatus() != NetworkStatus.ACTIVE){
				Thread.sleep(2000);	
			}	
			return subnetId;
		}catch (Exception e){
			logger.trace(e);
			throw new RuntimeErrorException(new Error(e.getMessage()));
		} 
	}

	private String getExternalAddressId() throws RuntimeErrorException{
		try{
			NetworkApi networkApi = neutronApi.getNetworkApi("fr1");
			List<Network> networkList = networkApi.list().concat().toList();
			for (Network network : networkList){
				if (network.getExternal()){
					return network.getId();
				}
			}
		}catch(Exception e){
			logger.debug("Cannot find the id of the external network");
			logger.trace(e);
			throw new RuntimeErrorException(new Error(e.getMessage()));
		}
		return null;
	}

	private boolean deleteGroupInstances(String groupName) throws RuntimeErrorException{
		try{
			compute.destroyNodesMatching(and(inGroup(groupName), not(TERMINATED)));
		}catch(Exception e){
			logger.debug("Cannot delete all the instances in the group : "+groupName);
			logger.trace(e);
			throw new RuntimeErrorException(new Error(e.getMessage()));
		}

		return true;
	}

	private boolean deallocateUnusedFloatingIP() throws RuntimeErrorException{
		try{
			FloatingIPApi floatingApi = neutronApi.getFloatingIPApi("fr1").get();
			List<FloatingIP> floatingList = floatingApi.list().concat().toList();
			for (FloatingIP floatingIP : floatingList){
				String getFixedIpAddress = floatingIP.getFixedIpAddress();

				if(getFixedIpAddress==null){
					floatingApi.delete(floatingIP.getId());
				}
			}
		}catch(Exception e){
			logger.debug("Cannot delete the floating IP");
			logger.trace(e);
			throw e;
		}
		return true;
	}

	private boolean clearGroupNodesAndNetwork(String routerName, String networkName, String groupName) throws RuntimeErrorException{
		try{
			deleteGroupInstances(groupName);
			deallocateUnusedFloatingIP();
			deleteSecurityGroup(groupName);
			deleteRouter(routerName, networkName);
			deleteNetwork(networkName);

			// TODO: remove the delete keypairs and deleteAllGroupsExceptDefault they are used only for testing
			deleteKeypair(groupName);

		}catch (Exception e){
			logger.trace(e);
			throw e;
		}

		return true;
	}

	private boolean clearGroupAndNetwork(String routerName, String networkName, String groupName) throws RuntimeErrorException{
		try{
			deallocateUnusedFloatingIP();
			deleteSecurityGroup(groupName);
			deleteRouter(routerName, networkName);
			deleteNetwork(networkName);

			//remove the delete keypairs and deleteAllGroupsExceptDefault they are used only for testing
			deleteKeypair(groupName+"Key");

		}catch (Exception e){
			logger.trace(e);
			throw e;
		}

		return true;
	}

	private void getPrivateAddress(String groupname) throws RuntimeErrorException{
		try{			

			Set<? extends ComputeMetadata> computeMetadata = compute.listNodes();
			ArrayList<String> idlist = new ArrayList<String>();
			for (ComputeMetadata c : computeMetadata){
				idlist.add(c.getId());
			}

			Thread.sleep(5000);
//			nodesupdated = compute.listNodesByIds(idlist);
			for(String nid : idlist){
				NodeMetadata nodo = compute.getContext().getComputeService().getNodeMetadata(nid);
				logger.debug("Nodo : "+nodo.getId()+ " Gruppo : "+nodo.getGroup());
				logger.debug("IP priv :"+nodo.getPrivateAddresses().toString());
				logger.debug("IP pub :"+nodo.getPublicAddresses().toString());
			}
		}catch (Exception e){
			logger.debug("Cannot remove the network");
			logger.trace(e);
			throw new RuntimeErrorException(new Error(e.getMessage()));
		}

	}

	@Override
	public NodesInfo getNodesByGroup(String groupname, NodeCredential nodeCredential) throws RuntimeErrorException{
		try{

			Set<? extends ComputeMetadata> computeMetadata = compute.listNodes();
			ArrayList<String> idlist = new ArrayList<String>();
			for (ComputeMetadata c : computeMetadata){
				idlist.add(c.getId());
			}

			Set<? extends NodeMetadata> nodesupdated = null;
			Thread.sleep(5000);
			nodesupdated = compute.listNodesByIds(idlist);
			NodesInfo info = new NodesInfo();

			for(NodeMetadata n : nodesupdated){
				if(n.getGroup()!=null && n.getGroup().equalsIgnoreCase(groupname)){

					info.addNode(new ClusterNode(n.getId(), 
							n.getPublicAddresses().isEmpty() ? "" : n.getPublicAddresses().iterator().next(),
									n.getPrivateAddresses().isEmpty() ? "" : n.getPrivateAddresses().iterator().next()
							));

				}
			}

			getPrivateAddress(groupname);

			info.setPublicKey(nodeCredential.getPublickey());
			info.setPrivateKey(nodeCredential.getPrivatekey());
			info.setRootPassword(nodeCredential.getRootpassword());
			return info;
		}catch (Exception e){
			logger.debug("Cannot retrive NodesInfo");
			logger.trace(e);
			throw new RuntimeErrorException(new Error(e.getMessage()));
		}
	}


	@Override
	public NodesInfo getNodesByGroup(String groupname) throws RuntimeErrorException{
		try{

			Set<? extends ComputeMetadata> computeMetadata = compute.listNodes();
			ArrayList<String> idlist = new ArrayList<String>();
			for (ComputeMetadata c : computeMetadata){
				idlist.add(c.getId());
			}

			Set<? extends NodeMetadata> nodesupdated = null;
			nodesupdated = compute.listNodesByIds(idlist);
			NodesInfo info = new NodesInfo();

			for(NodeMetadata n : nodesupdated){
				if(n.getGroup()!=null && n.getGroup().equalsIgnoreCase(groupname)){

					info.addNode(new ClusterNode(n.getId(), 
							n.getPublicAddresses().isEmpty() ? "" : n.getPublicAddresses().iterator().next(),
									n.getPrivateAddresses().isEmpty() ? "" : n.getPrivateAddresses().iterator().next()
							));

				}
			}

			getPrivateAddress(groupname);

			return info;
		}catch (Exception e){
			logger.debug("Cannot retrive NodesInfo");
			logger.trace(e);
			throw e;
		}
	}




}