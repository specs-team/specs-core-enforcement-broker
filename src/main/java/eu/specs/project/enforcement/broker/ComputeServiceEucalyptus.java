/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@developer Giancarlo Capone giancarlo.capone85@gmail.com
 */

package eu.specs.project.enforcement.broker;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.jclouds.compute.config.ComputeServiceProperties.TIMEOUT_SCRIPT_COMPLETE;
import static org.jclouds.compute.options.TemplateOptions.Builder.overrideLoginCredentials;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.management.RuntimeErrorException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jclouds.ContextBuilder;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.RunNodesException;
import org.jclouds.compute.callables.ScriptStillRunningException;
import org.jclouds.compute.domain.ComputeMetadata;
import org.jclouds.compute.domain.ExecResponse;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.compute.domain.Template;
import org.jclouds.compute.domain.TemplateBuilder;
import org.jclouds.compute.options.RunScriptOptions;
import org.jclouds.domain.LoginCredentials;
import org.jclouds.ec2.compute.options.EC2TemplateOptions;
import org.jclouds.scriptbuilder.ScriptBuilder;
import org.jclouds.scriptbuilder.domain.LiteralStatement;
import org.jclouds.scriptbuilder.domain.Statement;
import org.jclouds.scriptbuilder.statements.login.UserAdd;
import org.jclouds.scriptbuilder.statements.ssh.AuthorizeRSAPublicKeys;
import org.jclouds.scriptbuilder.statements.ssh.InstallRSAPrivateKey;
import org.jclouds.ssh.jsch.config.JschSshClientModule;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Module;

import eu.specs.datamodel.broker.BrokerTemplateBean;
import eu.specs.datamodel.broker.ClusterNode;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodesInfo;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.project.enforcement.broker.exceptions.ComputationException;
import eu.specs.project.enforcement.broker.exceptions.ImplementationException;
import eu.specs.project.enforcement.broker.interfaces.ComputeServiceInterface;
import eu.specs.project.enforcement.broker.utility.ScriptExecutionResult;
import eu.specs.project.enforcement.broker.utility.ScriptExecutionResult.ScriptExecutionOutcome;
import eu.specs.project.enforcement.contextlistener.ContextListener;


public class ComputeServiceEucalyptus implements ComputeServiceInterface{
	private ComputeServiceContext context;
	private ComputeService compute;
	private String defaultUser;
	private Template template;
	private static String EUCALYPTUS_KEYPAIR="rktest";
	private static final Logger logger = LogManager.getLogger(ComputeServiceEucalyptus.class);



	public ComputeServiceEucalyptus(String providerAddress, String defaultUser, ProviderCredential providerCredential){
		Properties properties = new Properties();
		long scriptTimeout = TimeUnit.MILLISECONDS.convert(20, TimeUnit.MINUTES);
		properties.setProperty(TIMEOUT_SCRIPT_COMPLETE, Long.toString(scriptTimeout));
		this.defaultUser = defaultUser;

		ContextBuilder builder = ContextBuilder.newBuilder("ec2")
				.credentials(providerCredential.getUsername(), providerCredential.getPassword())
				.overrides(properties)
				.modules(ImmutableSet.<Module> of(
						new JschSshClientModule()))
						//TODO toggle to enable/disable logs
						//						.modules(modules)
						//						.endpoint("http://194.102.62.130:8773/services/Eucalyptus")
						.endpoint(providerAddress);

		context =  builder.buildView(ComputeServiceContext.class);

		compute = context.getComputeService();
	}

	private Template obtainTemplate(BrokerTemplateBean descriptor, int... inboudports){
		TemplateBuilder templateBuilder = compute.templateBuilder();

		EC2TemplateOptions options = new EC2TemplateOptions();
		options.keyPair(EUCALYPTUS_KEYPAIR);
		options.inboundPorts(inboudports);
		options.blockOnPort(22, 20000);
		options.blockUntilRunning(true);
		String randomString = UUID.randomUUID().toString();
		String clusterId = ContextListener.getMosClusterId();

		if(clusterId==null)
			clusterId="ErrorRetrievingClusterId";
		
		randomString="specs"+randomString;
		logger.debug("the random string passed as userdata is: "+randomString);
		options.userData(("#!cluster:"+clusterId+":"+randomString+".nodes:mos-dummy-package").getBytes());

		template = templateBuilder
				.imageId("eucalyptus/"+descriptor.getAppliance())
				.hardwareId(descriptor.getHw())
				.locationId(descriptor.getZone())
				.options(options)
				.build();
		return template;
	}
	
	

	@Override
	public NodesInfo createNodesInGroup(String groupName, int numberOfInstances, 
			BrokerTemplateBean descriptor, NodeCredential nodeCredential,
			int... inboudports)  throws NoSuchElementException, Exception {


		Set<? extends NodeMetadata> nodes = null;
		try {
			Template t = obtainTemplate(descriptor, inboudports);
			nodes = compute.createNodesInGroup(groupName, numberOfInstances, t);

		}catch(NoSuchElementException e){
			logger.trace(e);
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			destroyClusterWithName(groupName);
			logger.trace(e);
			throw e;
		}

		NodesInfo info = new NodesInfo();
		for(NodeMetadata n : nodes){
			Iterator i = n.getPublicAddresses().iterator();

			info.addNode(new ClusterNode(n.getId(), i.next().toString(),i.next().toString()));
		}

		info.setPublicKey(nodeCredential.getPublickey());
		info.setPrivateKey(nodeCredential.getPrivatekey());
		info.setRootPassword(nodeCredential.getRootpassword());
		return info;
	}


	public ClusterNode addNode(String groupName, String pubKey, String privKey, String rootPassword) throws RunNodesException, RuntimeErrorException{
		NodeMetadata n = null;
		try{
			n = getOnlyElement(compute.createNodesInGroup(groupName, 1, template));
		} catch (RunNodesException e) {
			throw e;
		} finally{
			destroyClusterWithName(groupName);
		}
		HashSet<String> pubKeys = new HashSet<String>();
		pubKeys.add(pubKey);
		AuthorizeRSAPublicKeys authKeys = new AuthorizeRSAPublicKeys(pubKeys);
		InstallRSAPrivateKey instPriv = new InstallRSAPrivateKey(privKey);

		ScriptBuilder sb = new ScriptBuilder();

		sb.addStatement(authKeys);
		sb.addStatement(instPriv);
		ExecResponse er = compute.runScriptOnNode(n.getId(), sb, 
				overrideLoginCredentials(getLoginForCommandExecution(defaultUser, n.getCredentials().credential)).runAsRoot(false).wrapInInitScript(false));
		if(er.getExitStatus()!=0){
			destroyClusterWithName(groupName);
			throw new RuntimeErrorException(new Error("Error in configuration of custom RSA keys"));
		}

		LiteralStatement pass = new LiteralStatement(String.format("echo \"%s\" | passwd --stdin root", rootPassword));
		sb = new ScriptBuilder();
		sb.addStatement(pass);
		er = compute.runScriptOnNode(n.getId(), sb, 
				overrideLoginCredentials(getLoginForCommandExecution(defaultUser, n.getCredentials().credential)).runAsRoot(true).wrapInInitScript(false));
		if(er.getExitStatus()!=0){
			destroyClusterWithName(groupName);
			throw new RuntimeErrorException(new Error("Error in installing root password"));
		}

		return new ClusterNode(n.getId(), n.getPublicAddresses().iterator().next(), n.getPrivateAddresses().iterator().next());
	}


	@Override
	public ScriptExecutionResult addNewUser(String userName, String password, String newPublicKey, String newPrivateKey, List<ClusterNode> nodes, String privateKeyDefaultUser){
		HashMap<String, String> info = new HashMap<String, String>();
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		UserAdd.Builder userBuilder = UserAdd.builder();
		userBuilder.login(userName);
		userBuilder.authorizeRSAPublicKey(newPublicKey);
		userBuilder.installRSAPrivateKey(newPrivateKey);
		userBuilder.defaultHome("/home");
		if(password!=null){
			userBuilder.password(password);
		}
		Statement userBuilderStatement = userBuilder.build();
		ExecResponse er = null;

		for(ClusterNode n : nodes){
			er = compute.runScriptOnNode(n.getId(), userBuilderStatement,
					overrideLoginCredentials(getLoginForCommandExecution(defaultUser, privateKeyDefaultUser)).runAsRoot(true).wrapInInitScript(false)); 
			if(er.getExitStatus()!=0){
				scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
				scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
				scriptResult.addNodeWithError(n);
			}
		}
		info.put("privateKeyNewUser", newPrivateKey);
		info.put("publicKeyNewUser", newPublicKey);
		info.put("username", userName);
		info.put("password", password);
		scriptResult.setAdditionalInfo(info);
		return scriptResult;
	}

	@Override
	public ScriptExecutionResult executeScriptOnNode(String user, ClusterNode node, String[] instructions, String privateKey, boolean sudo)
			throws NoSuchElementException, IllegalStateException, ScriptStillRunningException{
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		ScriptBuilder sb = new ScriptBuilder();
		for(String s : instructions){
			sb.addStatement(new LiteralStatement(s));
		}


		final RunScriptOptions options=RunScriptOptions.Builder.overrideLoginCredentials(LoginCredentials.builder().privateKey(
				privateKey).user(user).noPassword().authenticateSudo(true).build()).wrapInInitScript(true);

		try {
			ExecResponse er = compute.runScriptOnNode(node.getId(), sb, options);

			if(er.getExitStatus()!=0){
				scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
				scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
				scriptResult.addNodeWithError(node);
			}
			scriptResult.addOutput(node.getId(), er.getOutput());
		} catch (Exception e) {
			throw e;
		}

		return scriptResult;
	}

	@Override
	public ScriptExecutionResult executeStatementOnNode(String user, ClusterNode node, Statement s, String privateKey, boolean sudo){

		try{
			ScriptExecutionResult scriptResult = new ScriptExecutionResult();
			ScriptBuilder sb = new ScriptBuilder();
			sb.addStatement(s);
			logger.debug("executeScriptOnNode - node id: "+node.getId());

			ExecResponse er = compute.runScriptOnNode(node.getId(), sb,
					RunScriptOptions.Builder.overrideLoginCredentials(LoginCredentials.builder().privateKey(
							privateKey).user(user).noPassword().authenticateSudo(true).build()));
			logger.debug("executeScriptOnNode - the runonscriptnode ha finished.");
			
			logger.debug("NodeId: "+node.getId()+" - exitStatus: "+er.getExitStatus());
			
			if(er.getExitStatus()!=0){
				scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
				scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
				scriptResult.addNodeWithError(node);

				logger.debug("executeScriptOnNode - er.getExitStatus(): "+er.getExitStatus());
				logger.debug("executeScriptOnNode - er.getError(): "+er.getError());
				logger.debug("executeScriptOnNode - er.getOutput(): "+er.getOutput());
//				throw new RuntimeException();
			}

			scriptResult.addOutput(node.getId(), er.getOutput());
			return scriptResult;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}


	}

	@Override
	public Map<String, ScriptExecutionResult> executeScriptOnNodes(String user, List<ClusterNode> nodes, String[] instructions, String privateKey, boolean sudo){
		Map<String, ScriptExecutionResult> map = new HashMap<String, ScriptExecutionResult>();
		ScriptExecutionResult temp;
		for(ClusterNode n : nodes){
			try {
				temp = executeScriptOnNode(user, n, instructions, privateKey, sudo);
				map.put(n.getId(), temp);
			} catch (Exception e) {
				logger.error(e);
			}

		}
		return map;
	}

	@Override
	public Map<String, ScriptExecutionResult> executeScriptOnNodes(String user, List<ClusterNode> nodes,Statement s, String privateKey, boolean sudo) throws ComputationException{
		Map<String, ScriptExecutionResult> map = new HashMap<String, ScriptExecutionResult>();
		ScriptExecutionResult temp;
		for(ClusterNode n : nodes){
			temp = executeStatementOnNode(user, n, s, privateKey, sudo);
			map.put(n.getId(), temp);
		}
		return map;
	}

	@Override
	public void suspendNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.suspendNode(hosts.get(i).getId());
		}
	}

	@Override
	public void resumeNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.resumeNode(hosts.get(i).getId());
		}
	}

	@Override
	public void rebootNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.rebootNode(hosts.get(i).getId());
		}
	}

	@Override
	public void destroyCluster(List<ClusterNode> hosts){ //distrugge anche keypair associati e il security group se non ci sono altre risorse pendenti associate
		for (int i = 0; i<hosts.size(); i++){
			compute.destroyNode(hosts.get(i).getId());
		}
	}

	@Override
	public void destroyClusterWithName(String groupName){
		if(groupName!=null && !groupName.startsWith("sla-")){
			groupName="sla-"+groupName.toLowerCase();
		}
		try{
			NodesInfo nodesInfo=null;
			nodesInfo=this.getNodesByGroup(groupName);

			if(nodesInfo!=null){
				logger.debug("trying to delete nodes... with groupname: "+groupName);
				this.destroyCluster(nodesInfo.getNodes());

			}

		} catch(Exception e){
			e.printStackTrace();
			logger.trace(e);
		}
	}

	@Override
	public void destroySingleNode(ClusterNode node){
		compute.destroyNode(node.getId());
	}

	private LoginCredentials getLoginForCommandExecution(String user, String credential) {

		return LoginCredentials.builder().user(user).credential(credential).build();   
	}


	public class executeIstructionsOnNode  extends Thread{

		private String user;
		private ClusterNode node;
		private String[] istructions;
		private String privateKey;
		boolean sudo;
		private ComputeServiceEucalyptus compute;

		public executeIstructionsOnNode(String user, ClusterNode node,
				String[] istructions, String privateKey, boolean sudo,
				ComputeServiceEucalyptus compute) {
			super();
			this.user = user;
			this.node = node;
			this.istructions = istructions;
			this.privateKey = privateKey;
			this.sudo = sudo;
			this.compute = compute;
		}

		@Override
		public void run(){ 
			try {
				compute.executeScriptOnNode(user, node,istructions,privateKey,sudo);
			} catch (Exception e) {
				logger.debug("first ssh connection to VM failed");
				logger.trace(e);
				long d = 10000;
				for(int i=0;i<10;i++){
					try {
						sleep(d);
						compute.executeScriptOnNode(user, node,istructions,privateKey,sudo);
						break;
					} catch (Exception e1) {
						logger.debug("ssh connection to VM failed");
						logger.trace(e1);
						if(i==9){
							logger.debug("ssh connection to VM failed");
							logger.trace(e1);
						}
					}
				}
			}

		}
	}

	public class ExecuteStatementsOnNode  extends Thread{

		private String user;
		private ClusterNode node;
		private Statement statement;
		private String privateKey;
		boolean sudo;
		private ComputeServiceEucalyptus compute;

		public ExecuteStatementsOnNode(String user, ClusterNode node,
				Statement statement, String privateKey, boolean sudo,
				ComputeServiceEucalyptus compute){
			super();
			this.user = user;
			this.node = node;
			this.statement = statement;
			this.privateKey = privateKey;
			this.sudo = sudo;
			this.compute = compute;
		}

		@Override
		public void run(){ 
			//			try {
			compute.executeStatementOnNode(user, node,statement,privateKey,sudo);
			//				Integer a=null;
			//				a++;
			//			} catch (Exception e) {
			//				logger.debug("si è verificato un catch nel metodo run di ExecuteIstructionsOnNode");
			//				logger.error(e);
			//				try {
			//					throw new ComputationException(e.getMessage());
			//				} catch (ComputationException e1) {
			//					e1.printStackTrace();
			//				}
			//			}

		}
	}

	@Override
	public void enableProvider(String providerEndpoint, String defaultUser,
			ProviderCredential providerCredential) throws ComputationException {

		/*
		 * empty method
		 */
	}

	@Override
	public NodesInfo getNodesByGroup(String groupname,
			NodeCredential nodeCredential) throws ComputationException {
		ComputeMetadata computeMetadata = compute.listNodes().iterator().next();
		computeMetadata.getId();
		return null;
	}

	@Override
	public NodesInfo getNodesByGroup(String groupname) throws ComputationException {
		Iterator iterator = compute.listNodes().iterator();
		NodesInfo nodesInfo = new NodesInfo();
		while(iterator.hasNext()){
			ComputeMetadata computeMetadata = (ComputeMetadata) iterator.next();
			ClusterNode node = new ClusterNode();
			if(computeMetadata.getName()!=null && computeMetadata.getName().startsWith(groupname)){
				logger.debug("computeMetadata name:d "+computeMetadata.getName());
				Iterator it = compute.getNodeMetadata(computeMetadata.getId()).getPublicAddresses().iterator();

				node.setId(computeMetadata.getId());

				if(it.hasNext()){

					node.setPublicIP(it.next().toString());
				}
				if(it.hasNext()){
					node.setPrivateIP(it.next().toString());
				}
				nodesInfo.addNode(node);
				logger.debug("id:"+computeMetadata.getId()+" - name: "+computeMetadata.getName()+
						" - publicIp: "+node.getPublicIP()+" - privateIp: "+node.getPrivateIP());
			}

		}

		return nodesInfo;
	}


	@Override
	public ClusterNode addNode(String groupName, NodeCredential nodeCredential,
			String privateAccessKey) throws RunNodesException {
		return null;
	}

	@Override
	public Thread threadExecuteStatementsOnNode(String user, ClusterNode node,
			Statement statement, String privateKey, boolean sudo) {
		return new ExecuteStatementsOnNode(user, node, statement, privateKey, sudo, this);

	}

	@Override
	public Thread threadExecuteInstructionsOnNode(String user,
			ClusterNode node, String[] istructions, String privateKey,
			boolean sudo) throws ComputationException {
		return new executeIstructionsOnNode(user, node, istructions, privateKey, sudo, this);
	}
}
