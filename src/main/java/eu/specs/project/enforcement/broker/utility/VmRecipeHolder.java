package eu.specs.project.enforcement.broker.utility;

import java.util.ArrayList;

public class VmRecipeHolder {

	private ArrayList<String> arrayListRecipes = new ArrayList<String>();
	private int position;

	public ArrayList<String> getArrayListRecipes() {
		return arrayListRecipes;
	}

	public void setArrayListRecipes(ArrayList<String> arrayListRecipes) {
		this.arrayListRecipes = arrayListRecipes;
	}
	
	public void addRecipestoList(String recipe){
		arrayListRecipes.add(recipe);
	}

	public void addRecipesToList(String recipes, String cookbook){
		for (String recipe : recipes.split(",")) {
			if (!recipe.contains("::")) {
				recipe = cookbook + "::" + recipe;
			}
			arrayListRecipes.add(recipe);
		}
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
		
}
