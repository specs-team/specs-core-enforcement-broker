package eu.specs.project.enforcement.broker.utility;

public class EnvironmentHolder {

	private String mos_cluster_identifier;

	public String getMos_cluster_identifier() {
		return mos_cluster_identifier;
	}

	public void setMos_cluster_identifier(String mos_cluster_identifier) {
		this.mos_cluster_identifier = mos_cluster_identifier;
	}
	
}
