package eu.specs.project.enforcement.broker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.broker.exceptions.ImplementationException;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;


public class EnforcementBrokerTest {
	private static String databagId = "a7452816dbd74eab928fe109b9516056";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass Called");

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		//do nothing
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("setUp Called");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown Called");
	}
	
	@Test
	public final void bootstrapChefTest(){
		System.out.println("bootstrapChefTest");
		String username = System.getenv("PROVIDER_USERNAME");
		String password = System.getenv("PROVIDER_PASSWORD");
		String endpoint = System.getenv("PROVIDER_ENDPOINT");
		System.out.println(username +" - "+ password +" - "+endpoint );
//		implementPlan();
	}
	

	private void implementPlan(){

		try {

			UUID idOne = UUID.randomUUID();
			System.out.println("UUID generato vale: "+idOne);
			databagId = idOne.toString();
			System.out.println("databagId vale: "+databagId);
			
			ImplementationPlan myplan = parsePlan(null);
			myplan.setId(databagId);

			System.out.println("myplan: "+myplan.toString());

			ObjectMapper mapper = new ObjectMapper();

			String jsonInString = mapper.writeValueAsString(myplan);
			System.out.println("jsonInString: "+jsonInString.toString());

			Provisioner p  = new Provisioner(System.getProperty("PROVIDER_USERNAME"), System.getProperty("PROVIDER_PASSWORD"), System.getProperty("PROVIDER_ENDPOINT"), "");
			p.deployVms(myplan);
			
			try {
				p.deleteVms("sla-"+myplan.getSlaId().toLowerCase(), "Eucalyptus");
			} catch (TimeoutException e) {
				e.printStackTrace();
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ImplementationException e) {
			e.printStackTrace();
		} 
	}


	private ImplementationPlan parsePlan(String planPath) throws IOException {
		System.out.println("parsePlan");
		ObjectMapper mapper = new ObjectMapper();
		ImplementationPlan myplan;
		System.out.println("prima del try");
		try {
			if(planPath==null){
				System.out.println("planPath==null");
				BufferedReader br = new BufferedReader(new InputStreamReader (Provisioner.class.getClassLoader().getResourceAsStream("plan_test.txt"), "UTF8")); 
				String value="";
				String tmp;
				while (( tmp = br.readLine()) != null) {
					value+=(tmp+"\n");
				}
				br.close();


				myplan = mapper.readValue(value, ImplementationPlan.class);
				System.out.println("myplan.getId(): "+myplan.getId());
				return myplan;
			}
			else{
				System.out.println("planPath!=null");
				Path path = Paths.get(planPath);
				String value="";
				Charset charset = Charset.forName("UTF-8");
				try (BufferedReader reader = Files.newBufferedReader(path, charset)) {
					String line = null;
					while ((line = reader.readLine()) != null) {
						System.out.println(line);
						value+=(line+"\n");
					}
				} catch (IOException x) {
					System.err.format("IOException: %s%n", x);
					throw x;
				}


				myplan = mapper.readValue(value.toString(), ImplementationPlan.class);
				System.out.println("myplan.getId(): "+myplan.getId());
				return myplan;
			}

		} catch (JsonParseException e) {
			e.printStackTrace();
			throw e;
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw e;
		}
	}




}
